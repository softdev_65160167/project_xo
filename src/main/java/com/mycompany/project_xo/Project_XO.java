/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.project_xo;

/**
 *
 * @author ADMIN
 */
import java.util.Scanner;
public class Project_XO {
    private static String[][] matrix = new String[3][3];
    static {
        matrix[0][0] = "1";
        matrix[0][1] = "2";
        matrix[0][2] = "3";
        matrix[1][0] = "4";
        matrix[1][1] = "5";
        matrix[1][2] = "6";
        matrix[2][0] = "7";
        matrix[2][1] = "8";
        matrix[2][2] = "9";
    }
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcom to OX Game");
        int count = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
         for(int i = 0 ; i < 5 ; i++){
            System.out.println("X Turn");
            System.out.print("Plase choose your Number:");
            int num = kb.nextInt();
            Change_X(num);
            count++;
            if(Check_Winner() == true){
                break;
            }
            if(count == 9){
                System.out.println("Draw!!!");
                break;
            }
            System.out.println("O Turn");
            System.out.print("Plase choose your Number:");
            int num1 = kb.nextInt();
            Change_O(num1);
            count++;
            if(Check_Winner() == true){
                break;
            }
        }

    }
    public static boolean Check_Winner() {
            //X_sleep
        if(matrix[0][0] == "X" && matrix[0][1] == "X" && matrix[0][2] == "X"){
            System.out.println("X is Winner!");
            return true;
        }else if(matrix[1][0] == "X" && matrix[1][1] == "X" && matrix[1][2] == "X"){
            System.out.println("X is Winner!");
            return true;
        }else if(matrix[2][0] == "X" && matrix[2][1] == "X" && matrix[2][2] == "X"){
            System.out.println("X is Winner!");
            return true;
            //X_Stand
        }else if(matrix[0][0] == "X" && matrix[1][0] == "X" && matrix[2][0] == "X"){
            System.out.println("X is Winner!");
            return true;
        }else if(matrix[0][1] == "X" && matrix[1][1] == "X" && matrix[2][1] == "X"){
            System.out.println("X is Winner!");
            return true;
        }else if(matrix[0][2] == "X" && matrix[1][2] == "X" && matrix[2][2] == "X"){
            System.out.println("X is Winner!");
            return true;
            //X_tayang
        }else if(matrix[0][0] == "X" && matrix[1][1] == "X" && matrix[2][2] == "X"){
            System.out.println("X is Winner!");
            return true;
        }else if(matrix[0][2] == "X" && matrix[1][1] == "X" && matrix[2][0] == "X"){
            System.out.println("X is Winner!");
            return true;
        }
        
        //O_Sleep
        else if(matrix[0][0] == "O" && matrix[0][1] == "O" && matrix[0][2] == "O"){
            System.out.println("O is Winner!");
            return true;
        }else if(matrix[1][0] == "O" && matrix[1][1] == "O" && matrix[1][2] == "O"){
            System.out.println("O is Winner!");
            return true;
        }else if(matrix[2][0] == "O" && matrix[2][1] == "O" && matrix[2][2] == "O"){
            System.out.println("O is Winner!");
            return true;
            //O_Stand
        }else if(matrix[0][0] == "O" && matrix[1][0] == "O" && matrix[2][0] == "O"){
            System.out.println("O is Winner!");
            return true;
        }else if(matrix[0][1] == "O" && matrix[1][1] == "O" && matrix[2][1] == "O"){
            System.out.println("O is Winner!");
            return true;
        }else if(matrix[0][2] == "O" && matrix[1][2] == "O" && matrix[2][2] == "O"){
            System.out.println("O is Winner!");
            return true;
            //O_tayang
        }else if(matrix[0][0] == "O" && matrix[1][1] == "O" && matrix[2][2] == "O"){
            System.out.println("O is Winner!");
            return true;
        }else if(matrix[0][2] == "O" && matrix[1][1] == "O" && matrix[2][0] == "O"){
            System.out.println("O is Winner!");
            return true;
        }
        return false;
    }
     static void Change_X(int num) {
        if(num == 1){
            matrix[0][0] = "X";
        } else if (num == 2) {
            matrix[0][1] = "X";
        } else if (num == 3) {
            matrix[0][2] = "X";
        } else if (num == 4) {
            matrix[1][0] = "X";
        } else if (num == 5) {
            matrix[1][1] = "X";
        } else if (num == 6) {
            matrix[1][2] = "X";
        } else if (num == 7) {
            matrix[2][0] = "X";
        } else if (num == 8) {
            matrix[2][1] = "X";
        } else if (num == 9) {
            matrix[2][2] = "X";
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
      }
     static void Change_O(int num1) {
        if(num1 == 1){
            matrix[0][0] = "O";
        } else if (num1 == 2) {
            matrix[0][1] = "O";
        } else if (num1 == 3) {
            matrix[0][2] = "O";
        } else if (num1 == 4) {
            matrix[1][0] = "O";
        } else if (num1 == 5) {
            matrix[1][1] = "O";
        } else if (num1 == 6) {
            matrix[1][2] = "O";
        } else if (num1 == 7) {
            matrix[2][0] = "O";
        } else if (num1 == 8) {
            matrix[2][1] = "O";
        } else if (num1 == 9) {
            matrix[2][2] = "O";
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
      }
}

